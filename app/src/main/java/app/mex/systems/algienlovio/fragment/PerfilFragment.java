package app.mex.systems.algienlovio.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import app.mex.systems.algienlovio.R;
import appservicios.mx.appservicios.Activity.LoginActivity;


public class PerfilFragment extends Fragment {
    private static View view;
    private TextView ColorAuto, CorreoUser, EstadoUser, MarcaUser, NombreUser, ModeloAuto, PlacaUser,
            SubMarcaAuto,UsoAuto;
    private Button Logaout;
    private FirebaseAuth mAuth;// ...
    private DatabaseReference mDatabase;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_perfil, container, false);
        init();

        Logaout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAuth.signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                    getActivity().finish();
            }
        });

        String id = mAuth.getCurrentUser().getUid();
        mDatabase.child("usuario").child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    String ColorModelo = dataSnapshot.child("colorModelo").getValue().toString();
                    String CorreoUsuario = dataSnapshot.child("correo").getValue().toString();
                    String EstadoUsuario = dataSnapshot.child("estado").getValue().toString();
                    String MarcaUsuario = dataSnapshot.child("marca").getValue().toString();
                    String ModeloUsuario = dataSnapshot.child("model").getValue().toString();
                    String NombreUsuario = dataSnapshot.child("nombre").getValue().toString();
                    String PlacaUsuario = dataSnapshot.child("placa").getValue().toString();
                    String SubMarcaUsuario = dataSnapshot.child("subMarca").getValue().toString();
                    String UsoUsuario = dataSnapshot.child("uso").getValue().toString();
                    String Contraseña = dataSnapshot.child("contraseña").getValue().toString();

                    ColorAuto.setText(ColorModelo);
                    CorreoUser.setText(CorreoUsuario);
                    EstadoUser.setText(EstadoUsuario);
                    MarcaUser.setText(MarcaUsuario);
                    NombreUser.setText(NombreUsuario);
                    ModeloAuto.setText(ModeloUsuario);
                    PlacaUser.setText(PlacaUsuario);
                    SubMarcaAuto.setText(SubMarcaUsuario);
                    UsoAuto.setText(UsoUsuario);


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast toast1 =
                        Toast.makeText(getContext(),
                                "Toast por defecto"+databaseError, Toast.LENGTH_SHORT);

                toast1.show();
            }
        });


        return view;
    }



    private void init(){
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        ColorAuto = (TextView) view.findViewById(R.id.txtColor);
        CorreoUser = (TextView) view.findViewById(R.id.textView4);
        EstadoUser = (TextView) view.findViewById(R.id.txtEstado);
        MarcaUser = (TextView) view.findViewById(R.id.txtMarca);
        NombreUser = (TextView) view.findViewById(R.id.NombreUser);
        ModeloAuto = (TextView) view.findViewById(R.id.txtModelo);
        PlacaUser = (TextView) view.findViewById(R.id.NumeroPlaca);
        SubMarcaAuto = (TextView) view.findViewById(R.id.txtSubModel);
        UsoAuto = (TextView) view.findViewById(R.id.tipoUso);
        Logaout = (Button) view.findViewById(R.id.BtnCerrar);
    }


}
