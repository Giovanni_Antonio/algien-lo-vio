package app.mex.systems.algienlovio.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.res.ColorStateList;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import app.mex.systems.algienlovio.R;
import app.mex.systems.algienlovio.Utils.CustomToast;
import app.mex.systems.algienlovio.Utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUp_Fragment extends Fragment implements OnClickListener {
	private static View view;
	private static EditText  emailId, password, confirmPassword, Nombre,NumeroPlaca,Modelo,Color;
	private static TextView login, txtAviso;
	private static Button signUpButton;
	private static CheckBox terms_conditions;
	private ProgressDialog progressDialog;
	private FirebaseAuth mAuth;// ...
	private DatabaseReference mDatabase;
    private String Name="";
    private String Email="";
    private String Password="";
    private Spinner spinnerEstado, spinnerMarcas, spinnerSubMarca,spinnerUso;
    private ImageView imagePlaca;
	private BottomSheetBehavior sheetBehavior;
	private ConstraintLayout layoutBottomSheet;


	public SignUp_Fragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_registro, container, false);
		initViews();
		setListeners();

     ArrayAdapter<CharSequence>adapter = ArrayAdapter.createFromResource(getContext(),R.array.Marcas,android.R.layout.simple_spinner_dropdown_item );
     adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
     spinnerMarcas.setAdapter(adapter);
     spinnerMarcas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		 @Override
		 public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
			 int[]submarcas ={R.array.Abarth,R.array.Acura,R.array.Alfa,R.array.Aston,R.array.Audi,R.array.BAIC,
					 R.array.Bentley,R.array.BMW,R.array.Buick,R.array.Cadillac,R.array.Chang,R.array.Chevrolet,
					 R.array.Chrysler, R.array.Dodge,R.array.DFSK,R.array.FAW,R.array.Ferrari,R.array.Fiat,
					 R.array.Ford,R.array.GMC,R.array.Honda,R.array.Hyundai,R.array.Infiniti,R.array.JAC,
					 R.array.Jaguar,R.array.Jeep,R.array.Kia,R.array.Lamborghini,R.array.LandRover,
					 R.array.Lincoln,R.array.Lotus,R.array.maserati,R.array.Mazda,R.array.McLaren,R.array.Mercedes,
					 R.array.MINI,R.array.Mitsubishi,R.array.Nissan,R.array.Peugeot,R.array.Porsche,R.array.Ram,
					 R.array.Renault,R.array.Rolls,R.array.SEAT,R.array.Smart,R.array.SRT,R.array.Subaru,
					 R.array.Suzuki,R.array.Tesla,R.array.Toyota,R.array.Volkswagen,R.array.Volvo,R.array.VuHL};

			 ArrayAdapter<CharSequence>adapter = ArrayAdapter.createFromResource(getActivity(),submarcas[position],android.R.layout.simple_spinner_item);
			 adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			 spinnerSubMarca.setAdapter(adapter);
		 }

		 @Override
		 public void onNothingSelected(AdapterView<?> parent) {

		 }
	 });

		ArrayAdapter<CharSequence>adapterPlacas = ArrayAdapter.createFromResource(getContext(),R.array.Estados,android.R.layout.simple_spinner_dropdown_item );
		adapterPlacas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerEstado.setAdapter(adapterPlacas);
		spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				switch (position){
					case 0:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 1:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 2:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 3:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 4:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 5:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 6:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 7:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 8:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 9:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 10:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 11:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 12:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 13:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 14:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 15:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 16:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 17:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 18:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 19:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 20:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 21:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 22:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 23:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 24:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 25:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 26:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 27:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 28:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 29:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
					case 30:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.cdmx));
						break;
					case 31:
						imagePlaca.setBackgroundDrawable(getResources().getDrawable(R.drawable.estadomex));
						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		return view;
	}

	// Initialize all views
	private void initViews() {
		layoutBottomSheet = view.findViewById(R.id.bottom_sheet);
		emailId = (EditText) view.findViewById(R.id.etv_user);
		password = (EditText) view.findViewById(R.id.etv_pass);
		confirmPassword = (EditText) view.findViewById(R.id.etv_pass);
		NumeroPlaca= (EditText) view.findViewById(R.id.NumeroPlaca);
		Modelo = (EditText) view.findViewById(R.id.modelo);
		Color = (EditText) view.findViewById(R.id.color);
		signUpButton = (Button) view.findViewById(R.id.btn_aceptar);
		Nombre =(EditText) view.findViewById(R.id.etv_nombre);
		terms_conditions = (CheckBox) view.findViewById(R.id.checkBoxAceptar);
		progressDialog = new ProgressDialog(getActivity());
		mAuth = FirebaseAuth.getInstance();
		mDatabase = FirebaseDatabase.getInstance().getReference();
		spinnerEstado = view.findViewById(R.id.SpinnerEstado);
        spinnerMarcas = view.findViewById(R.id.spinnerMarcas);
        spinnerSubMarca = view.findViewById(R.id.spinnerSubMarca);
        spinnerUso = view.findViewById(R.id.spinnerTipo);
        imagePlaca = view.findViewById(R.id.ImagePlaca);


		sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
		txtAviso = view.findViewById(R.id.txt_aviso);
		txtAviso.setOnClickListener(this);

		// Setting text selector over textviews
		@SuppressLint("ResourceType") XmlResourceParser xrp = getResources().getXml(R.drawable.text_selector);
		try {
			ColorStateList csl = ColorStateList.createFromXml(getResources(), xrp);


			terms_conditions.setTextColor(csl);
		} catch (Exception e) {
		}
	}


	// Set Listeners
	private void setListeners() {
		signUpButton.setOnClickListener(this);


	}



	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_aceptar:

			// Call checkValidation method
			checkValidation();
			break;
			case R.id.txt_aviso:
				if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
					sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
				} else {
					sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
				}
				break;
		/**case R.id.btnLogin:

			// Replace login fragment
			new LoginActivity().replaceLoginFragment();
			break;**/

			/**case R.id.campoNombre:
                new Retum().withActivity((AppCompatActivity) getActivity())
                        .setPredictor(new CnicPredictor())
                        .scan(this);

				break;**/
		}

	}

	private void getUserInfo(){
		String id = mAuth.getCurrentUser().getUid();
		mDatabase.child("usuario").child(id).addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				if (dataSnapshot.exists()){
					String Placas = dataSnapshot.child("Placa").getValue().toString();
					NumeroPlaca.setText(Placas);
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {

			}
		});
	}

	// Check Validation Method
	private void checkValidation() {

		// Get all edittext texts
		final String getEmailId = emailId.getText().toString();
		final String getPassword = password.getText().toString();
		String getConfirmPassword = confirmPassword.getText().toString();

		Name = Nombre.getText().toString();

		// Pattern match for email id
		Pattern p = Pattern.compile(Utils.regEx);
		Matcher m = p.matcher(getEmailId);

		// Check if all strings are null or not
		if (getEmailId.equals("") || getEmailId.length() == 0
				|| getPassword.equals("") || getPassword.length() == 0
				|| getConfirmPassword.equals("")
				|| getConfirmPassword.length() == 0)

			new CustomToast().Show_Toast(getActivity(), view,
					"Todos los campos son obligatorios.");

		// Check if email id valid or not
		else if (!m.find())
			new CustomToast().Show_Toast(getActivity(), view,
					"Tu correo electrónico no es válido");
		// Check if both password should be equal
		else if (!getConfirmPassword.equals(getPassword))
			new CustomToast().Show_Toast(getActivity(), view,
							"Ambas contraseñas no coinciden.");

		// Make sure user should check Terms and Conditions checkbox
		else if (!terms_conditions.isChecked()) {
			new CustomToast().Show_Toast(getActivity(), view,
					"Por favor seleccione Términos y Condiciones.");

			progressDialog.setMessage("Realizando registro en linea...");
			progressDialog.show();
			// Else do signup or do your stuff
		}else {
			mAuth.createUserWithEmailAndPassword(getEmailId, getConfirmPassword)
					.addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(@NonNull Task<AuthResult> task) {
							if (task.isSuccessful()) {
								// Sign in success, update UI with the signed-in user's information
								Toast.makeText(getActivity(),"Se ha registrado el usuario con el email: "+ emailId.getText(),Toast.LENGTH_LONG).show();
								FirebaseUser user = mAuth.getCurrentUser();
								String Placa=NumeroPlaca.getText().toString();
								String Estado=spinnerEstado.getSelectedItem().toString();
								String Marca=spinnerMarcas.getSelectedItem().toString();
								String SubMarca=spinnerSubMarca.getSelectedItem().toString();
								String Model= Modelo.getText().toString();
								String ColorModelo= Color.getText().toString();
								String Uso=spinnerUso.getSelectedItem().toString();

								Map<String, Object> map = new HashMap<>();
								map.put("placa",Placa);
								map.put("estado",Estado);
								map.put("marca",Marca);
								map.put("subMarca",SubMarca);
								map.put("model",Model);
								map.put("colorModelo",ColorModelo);
								map.put("uso",Uso);
								map.put("nombre",Name);
								map.put("correo",getEmailId);
								map.put("contraseña",getPassword);
								String id= mAuth.getCurrentUser().getUid();
								mDatabase.child("usuario").child(id).setValue(map).addOnCompleteListener(new OnCompleteListener<Void>() {
									@Override
									public void onComplete(@NonNull Task<Void> task2) {
										if (task2.isSuccessful()) {
											Toast.makeText(getActivity(),"Cuenta ya registrada x 2", Toast.LENGTH_LONG).show();

										}

									}
								});

							} else{
								if (task.getException()instanceof FirebaseAuthUserCollisionException){
									Toast.makeText(getActivity(),"Cuenta ya registrada ", Toast.LENGTH_LONG).show();
								}else{
									Toast.makeText(getActivity(),"No se pudo registrar el usuario ", Toast.LENGTH_LONG).show();
								}


							}
							progressDialog.dismiss();

							// ...
						}
					});
		}

	}

}
