package app.mex.systems.algienlovio.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import app.mex.systems.algienlovio.R;
import spencerstudios.com.ezdialoglib.EZDialog;
import spencerstudios.com.ezdialoglib.EZDialogListener;

import static android.app.Activity.RESULT_OK;

public class AuxiliaRFragment extends Fragment implements View.OnClickListener {
Button BtnCamera,BtnEnviar;
ImageView imageView;
EditText textInformacion;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_auxiliar, container, false);
        textInformacion = (EditText)view.findViewById(R.id.modelo);
        BtnCamera= (Button) view.findViewById(R.id.btnCamera);
        BtnEnviar =(Button)view.findViewById(R.id.btnEnviar);
        imageView =(ImageView)view.findViewById(R.id.imageView2);
        BtnCamera.setOnClickListener(this);
        BtnEnviar.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnCamera:
                CameraIntent();
                break;
            case R.id.btnEnviar:
                new EZDialog.Builder(getActivity())
                        .setTitle("Envio")
                        .setMessage("La informacion que pusiste es correcta?")
                        .setPositiveBtnText("enviar")
                        .setNegativeBtnText("cancelar")
                        .setCancelableOnTouchOutside(true)
                        .OnPositiveClicked(new EZDialogListener() {
                            @Override
                            public void OnClick() {
                                //todo
                            }
                        })
                        .OnPositiveClicked(new EZDialogListener() {
                            @Override
                            public void OnClick() {
                                new EZDialog.Builder(getActivity())
                                        .setTitle("Envio")
                                        .setMessage("Envio exitoso")
                                        .build();
                                imageView.setImageResource(0);
                                textInformacion.setText("");                               }
                        })
                        .build();
        }
    }

    public void CameraIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null){
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    }
}
