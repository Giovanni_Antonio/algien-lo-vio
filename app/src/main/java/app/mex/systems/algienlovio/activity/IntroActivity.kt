package appservicios.mx.appservicios.Activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import app.mex.systems.algienlovio.R
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment

class IntroActivity : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        addSlide(AppIntroFragment.newInstance("EDMTDev",
            "Coffe", R.drawable.ic_camera,
            Color.parseColor("#673AB7")))

        addSlide(AppIntroFragment.newInstance("EDMTDev",
            "Snack ",R.drawable.ic_camera,
            Color.parseColor("#5E35B1")))

        addSlide(AppIntroFragment.newInstance("EDMTDev",
            "cup Cake",R.drawable.ic_camera,
            Color.parseColor("#512DA8")))

        addSlide(AppIntroFragment.newInstance("EDMTDev",
            "Coffe",R.drawable.ic_camera,
            Color.parseColor("#4527A0")))

        showStatusBar(false)
        setBarColor(Color.parseColor("#00ff0000"))
        setSeparatorColor(Color.parseColor("#00ff0000"))
    }

    override fun onDonePressed() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish ()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
    }

    override fun onSlideChanged() {
    }
}
