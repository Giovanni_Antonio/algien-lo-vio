package app.mex.systems.algienlovio.activity

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import app.mex.systems.algienlovio.R
import app.mex.systems.algienlovio.fragment.*
import com.iammert.library.readablebottombar.ReadableBottomBar
import kotlinx.android.synthetic.main.activity_main.*
import spencerstudios.com.ezdialoglib.EZDialog

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        readableBottomBar.setOnItemSelectListener(object :ReadableBottomBar.ItemSelectListener{
            override fun onItemSelected(index: Int) {
                var fragmentManager = getSupportFragmentManager()
                var fragment = Fragment()
                when(index){
                    0 ->{
                        EZDialog.Builder(this@MainActivity)
                            .setTitle(getString(R.string.reporte))
                            .setMessage(getString(R.string.mensaje))
                            .setTitleTextColor(Color.RED )
                            .setMessageTextColor(Color.BLACK)
                            .setCancelableOnTouchOutside(true)
                            .build()

                    }
                    1->{
                        val auxiliarfragment = AuxiliaRFragment()
                        fragmentManager.beginTransaction().replace(R.id.containerFragment, auxiliarfragment).commit()
                    }
                    2->{
                        val historialFragment = HistorialFragment()
                        fragmentManager.beginTransaction().replace(R.id.containerFragment, historialFragment).commit()
                    }
                    3->{
                        val perfilFragment = PerfilFragment()
                        fragmentManager.beginTransaction().replace(R.id.containerFragment, perfilFragment).commit()
                    }
                    4->{
                        val reportefragment = ReportarFragment()
                        fragmentManager.beginTransaction().replace(R.id.containerFragment, reportefragment).commit()
                    }

                }
            }
        })
    }
}
