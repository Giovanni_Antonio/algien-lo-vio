package app.mex.systems.algienlovio.activity

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.v4.app.FragmentActivity
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import app.mex.systems.algienlovio.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import kotlinx.android.synthetic.main.activity_registro.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthUserCollisionException


/**class RegistroActivity : AppCompatActivity() {
    //Declaramos un objeto firebaseAuth
    private var firebaseAuth: FirebaseAuth? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)


        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance()
        progressDialog = ProgressDialog(this)

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.Marcas,
            android.R.layout.simple_spinner_item
        )

        val adapterImage = ArrayAdapter.createFromResource(this,R.array.Estados,android.R.layout.simple_spinner_dropdown_item)
        SpinnerEstado.adapter=adapterImage
        SpinnerEstado.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {


                when (position) {
                    0 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    1 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    2 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    3 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    4 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    5 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    6 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    7 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    8 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    9 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    10 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    11 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    12 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    13 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    14 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    15 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    16 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    17 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    18 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    19 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    20 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    21 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    22 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    23 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    24 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    25 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    26 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    27 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    28 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    29 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                    30 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.cdmx))
                    31 -> ImagePlaca.setBackgroundDrawable(resources.getDrawable(R.drawable.estadomex))
                }

            }
        }
        /**Adaptador para el estado**/


        /** Adaptador para el spinner Marcas**/
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
        spinnerMarcas.adapter=adapter
        spinnerMarcas.onItemSelectedListener = object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val SubMarcas = arrayOf(R.array.Abarth,R.array.Acura,R.array.Alfa,R.array.Aston,R.array.Audi,R.array.BAIC,
                    R.array.Bentley,R.array.BMW,R.array.Buick,R.array.Cadillac,R.array.Chang,R.array.Chevrolet,
                    R.array.Chrysler, R.array.Dodge,R.array.DFSK,R.array.FAW,R.array.Ferrari,R.array.Fiat,
                    R.array.Ford,R.array.GMC,R.array.Honda,R.array.Hyundai,R.array.Infiniti,R.array.JAC,
                    R.array.Jaguar,R.array.Jeep,R.array.Kia,R.array.Lamborghini,R.array.LandRover,
                    R.array.Lincoln,R.array.Lotus,R.array.maserati,R.array.Mazda,R.array.McLaren,R.array.Mercedes,
                    R.array.MINI,R.array.Mitsubishi,R.array.Nissan,R.array.Peugeot,R.array.Porsche,R.array.Ram,
                    R.array.Renault,R.array.Rolls,R.array.SEAT,R.array.Smart,R.array.SRT,R.array.Subaru,
                    R.array.Suzuki,R.array.Tesla,R.array.Toyota,R.array.Volkswagen,R.array.Volvo,R.array.VuHL)
                val adapter = ArrayAdapter.createFromResource(
                    this@RegistroActivity, SubMarcas[position],
                    android.R.layout.simple_spinner_item)
                adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line)
                spinnerSubMarca.adapter=adapter               }

        }

        btn_aceptar.setOnClickListener { registrarUsuario()}
    }

    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = firebaseAuth!!.getCurrentUser()

    }

    private fun registrarUsuario() {

        //Obtenemos el email y la contraseña desde las cajas de texto
        val email = etv_user.getText().toString().trim()
        val password = etv_pass.getText().toString().trim()

        //Verificamos que las cajas de texto no esten vacías
        if (TextUtils.isEmpty(email)) {
            Toast.makeText(this, "Se debe ingresar un email", Toast.LENGTH_LONG).show()
            return
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, "Falta ingresar la contraseña", Toast.LENGTH_LONG).show()
            return
        }

        progressDialog!!.setMessage("Realizando registro en linea...")
        progressDialog!!.show()

        //creating a new user
        firebaseAuth!!.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
                override fun onComplete(@NonNull task: Task<AuthResult>) {
                    //checking if success
                    if (task.isSuccessful) {

                        Toast.makeText(
                            this@RegistroActivity,
                            "Se ha registrado el usuario con el email: " + email,
                            Toast.LENGTH_LONG
                        ).show()
                        val user = firebaseAuth!!.getCurrentUser()
                    } else {
                        if (task.exception is FirebaseAuthUserCollisionException) {
                            Toast.makeText(this@RegistroActivity, "Cuenta ya registrada ", Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this@RegistroActivity, "No se pudo registrar el usuario ", Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                    progressDialog!!.dismiss()
                }
            })

    }


}
**/