package appservicios.mx.appservicios.Activity


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import app.mex.systems.algienlovio.R
import app.mex.systems.algienlovio.Utils.Utils
import app.mex.systems.algienlovio.fragment.Login_Fragment

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private var fragmentManager: FragmentManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        fragmentManager = supportFragmentManager

        // If savedinstnacestate is null then replace login fragment
        if (savedInstanceState == null) {
            fragmentManager!!.beginTransaction().replace(R.id.frameContainer, Login_Fragment(), Utils.Login_Fragment).commit()
        }

        val thread = Thread(Runnable {
            val getPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
            val isFristStart = getPrefs.getBoolean("fristStart", true)
            if (isFristStart) {
                startActivity(Intent(this@LoginActivity, IntroActivity::class.java))
                val e = getPrefs.edit()
                e.putBoolean("fristStart", false)
                e.apply()
            }
        })
        thread.start()
    }

    override fun onClick(v: View?) {

        /*when (v!!.id) {
            R.id.btnSignup ->startActivity(Intent(this@LoginActivity, RegistroActivity::class.java))
            R.id.btn_login -> startActivity(Intent(this@LoginActivity, ServiciosActivity::class.java))
            R.id.fbButton -> muestraLoginConProveedores()
            R.id.GoogButton -> muestraLoginConProveedores()
        }*/
    }





    companion object {

        private const val RC_SIGN_IN = 123
    }

    // Replace Login Fragment with animation
    fun replaceLoginFragment() {


        fragmentManager?.beginTransaction()?.setCustomAnimations(R.anim.left_enter, R.anim.right_out)
            ?.replace(R.id.frameContainer, Login_Fragment(), Utils.Login_Fragment)?.commit()

        /**fragmentManager!!.beginTransaction().setCustomAnimations(R.anim.left_enter, R.anim.right_out)
        .replace(R.id.frameContainer, Login_Fragment(), Utils.Login_Fragment).commit()**/
    }

    override fun onBackPressed() {

        // Find the tag of signup and forgot password fragment
        val SignUp_Fragment = fragmentManager!!.findFragmentByTag(Utils.SignUp_Fragment)
        val ForgotPassword_Fragment = fragmentManager!!.findFragmentByTag(Utils.ForgotPassword_Fragment)

        // Check if both are null or not
        // If both are not null then replace login fragment else do backpressed
        // task

        if (SignUp_Fragment != null)
            replaceLoginFragment()
        else if (ForgotPassword_Fragment != null)
            replaceLoginFragment()
        else
            super.onBackPressed()
    }

}
